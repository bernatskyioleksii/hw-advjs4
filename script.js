//Теоретичне питання

//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

//Основна перевага AJAX полягає в тому, що він дозволяє оновлювати окремі частини вебсторінки без зміни всього її вмісту.
// Це покращує швидкість та реагування вебдодатків.



// Завдання

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// 1 Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

// 2 Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
// Список персонажів можна отримати з властивості characters.

// 3 Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
// Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).

// 4 Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму,
// вивести цю інформацію на екран під назвою фільму.





// Виконати AJAX запит для отримання списку фільмів серії "Зоряні війни"

const filmsUrl = "https://ajax.test-danit.com/api/swapi/films";

// Функція для отримання персонажів фільму та їх виведення на екран
function fetchCharactersAndDisplay(film, filmItem) {
    const characterPromises = film.characters.map((characterUrl) =>
        fetch(characterUrl).then((response) => response.json())
    );

    Promise.all(characterPromises)
        .then((characters) => {
            const characterNames = characters.map((character) => character.name);

            const charactersList = document.createElement('ul');
            charactersList.innerHTML = 'Characters';

            characterNames.forEach((characterName) => {
                const characterItem = document.createElement('li');
                characterItem.textContent = characterName;

                charactersList.append(characterItem);
            });

            filmItem.append(charactersList);
        })
        .catch((error) => {
            console.log("Виникла помилка при отриманні списку персонажів:", error);
        });
}

// Виконати запит до API для отримання списку фільмів
fetch(filmsUrl)
    .then((response) => response.json())
    .then((filmsResponse) => {
        const films = filmsResponse;

        const filmsList = document.createElement('ul');
        filmsList.innerHTML = 'Films';

        films.forEach((film) => {
            const filmItem = document.createElement('li');
            filmItem.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}`;

            filmsList.append(filmItem);

            fetchCharactersAndDisplay(film, filmItem);
        });

        document.body.append(filmsList);
    })
    .catch((error) => {
        console.log("Виникла помилка при отриманні списку фільмів:", error);
    });

